<div id="content">
	<div class="row">
		<h1><?php $this->info("company_name"); ?></h1>
		<h3>CARPETING, PAW PAW, MI</h3>
		<h4>We serve all southwest Michigan and northern Indiana</h4>
		<p class="homePar">With Many years experience, JV’s Carpeting, & Flooring works hard to bring each customer the best possible service and quality available. We look forward to working with you soon, on your next project. We welcome your questions and thank you for your interest. We have many references and pictures available upon request</p>
		<a href="services#content" class="btn">READ MORE</a>
	</div>
</div>
<div id="section2">
	<div class="row">
		<h2>We Offer</h2>
		<h4>installation services including but not limited to</h4>
		<h5>Please call to set up an appointment for a free estimate.</h5>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/we-offer1.jpg" alt="laminate"> </dt>
				<dd>LAMINATE</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/we-offer2.jpg" alt="lvt"> </dt>
				<dd>LVT</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/we-offer3.jpg" alt="lvp"> </dt>
				<dd>LVP</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/we-offer4.jpg" alt="tile"> </dt>
				<dd>TILE</dd>
			</dl>
		</div>
		<div class="container2">
			<dl>
				<dt> <img src="public/images/content/we-offer5.jpg" alt="carpet"> </dt>
				<dd>CARPET</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/we-offer6.jpg" alt="hard wood"> </dt>
				<dd>HARD WOOD</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/we-offer7.jpg" alt="Vinyls planks"> </dt>
				<dd>VINYLS PLANKS</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section3">
	<div class="row">
		<div class="container">
			<img src="public/images/content/testiHeader.png" alt="left quote">
			<p class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
			<p class="text">John and his company were fantastic during every step of this big project. The floor looks beautiful. They went above and beyond and were wonderful. I would definitely have them come back. I recommend them highly.</p>
			<p class="auth">- PATRICIA C. </p>
			<p class="sm">
				<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
				<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
				<a href="<?php $this->info("yt_link"); ?>" class="socialico">x</a>
				<a href="<?php $this->info("rs_link"); ?>" class="socialico">r</a>
			</p>
		</div>
		<div class="container2">
			<h1>Contact Us</h1>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<div class="cntTop">
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
				</div>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message/Questions:"></textarea>
				</label>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
			</form>
		</div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="container">
			<div class="item">
				<h2>Recent <span>Projects</span> </h2>
				<a href="gallery#content" class="btn">VIEW MORE</a>
			</div>
			<img src="public/images/content/project1.jpg" alt="floor">
			<img src="public/images/content/project2.jpg" alt="stairs">
			<img src="public/images/content/project3.jpg" alt="stairs">
		</div>
	</div>
</div>
